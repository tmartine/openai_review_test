import logging
import os
import typing

import gitlab
import openai
import tiktoken

logging.basicConfig(level=logging.INFO)

MAX_TOKENS_PER_REQUEST = 4097

GITLAB_TOKEN = os.environ["GITLAB_TOKEN_GPT_4"]

MODELS: list[dict[str, str]] = [
    {"name": "gpt-4", "gitlab_token": GITLAB_TOKEN},
    {"name": "gpt-3.5-turbo", "gitlab_token": os.environ["GITLAB_TOKEN_GPT_3_5"]},
]

CI_PROJECT_ID = os.environ["CI_PROJECT_ID"]
CI_MERGE_REQUEST_IID = os.environ["CI_MERGE_REQUEST_IID"]
OPENAI_API_KEY = os.environ["OPENAI_API_KEY"]

openai.api_key = OPENAI_API_KEY


def get_mr(gitlab_token):
    gl = gitlab.Gitlab("https://gitlab.inria.fr", private_token=gitlab_token)
    project = gl.projects.get(CI_PROJECT_ID)
    mr = project.mergerequests.get(CI_MERGE_REQUEST_IID)
    return mr


def fetch_code_for_review() -> str:
    mr = get_mr(GITLAB_TOKEN)
    code_for_review = ""
    return "\n--commit--\n".join(
        commit.message
        + "\n"
        + "\n".join(
            f"Diff for file {diff['old_path']} -> {diff['new_path']}\n{diff['diff']}"
            for diff in commit.diff()
        )
        for commit in mr.commits()
    )


def query_openai(model, messages, max_tokens) -> str:
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        max_tokens=max_tokens,
    )
    return response["choices"][0]["message"]["content"]


def post_feedback(gitlab_token, feedback) -> None:
    mr = get_mr(gitlab_token)
    mr.notes.create({"body": feedback})


# From https://github.com/openai/openai-cookbook/blob/main/examples/How_to_count_tokens_with_tiktoken.ipynb
def num_tokens_from_messages(messages, model) -> int:
    """Return the number of tokens used by a list of messages."""
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        print("Warning: model not found. Using cl100k_base encoding.")
        encoding = tiktoken.get_encoding("cl100k_base")
    if model in {
        "gpt-3.5-turbo-0613",
        "gpt-3.5-turbo-16k-0613",
        "gpt-4-0314",
        "gpt-4-32k-0314",
        "gpt-4-0613",
        "gpt-4-32k-0613",
    }:
        tokens_per_message = 3
        tokens_per_name = 1
    elif model == "gpt-3.5-turbo-0301":
        tokens_per_message = (
            4  # every message follows <|start|>{role/name}\n{content}<|end|>\n
        )
        tokens_per_name = -1  # if there's a name, the role is omitted
    elif "gpt-3.5-turbo" in model:
        print(
            "Warning: gpt-3.5-turbo may update over time. Returning num tokens assuming gpt-3.5-turbo-0613."
        )
        return num_tokens_from_messages(messages, model="gpt-3.5-turbo-0613")
    elif "gpt-4" in model:
        print(
            "Warning: gpt-4 may update over time. Returning num tokens assuming gpt-4-0613."
        )
        return num_tokens_from_messages(messages, model="gpt-4-0613")
    else:
        raise NotImplementedError(
            f"""num_tokens_from_messages() is not implemented for model {model}. See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens."""
        )
    num_tokens = 0
    for message in messages:
        num_tokens += tokens_per_message
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":
                num_tokens += tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens


def main() -> None:
    code_for_review = fetch_code_for_review()
    messages = [
        {
            "role": "user",
            "content": f"Comment the following merge-request and make a code review. The contents of the merge-request is presented below as a list of commits: for each commit, the commit message and diff output with + and - signs is given. \n{code_for_review}\n",
        }
    ]
    for model in MODELS:
        encoding = tiktoken.encoding_for_model(model["name"])
        max_tokens = MAX_TOKENS_PER_REQUEST - num_tokens_from_messages(
            messages, model["name"]
        )
        if max_tokens <= 0:
            return
        feedback = query_openai(model["name"], messages, max_tokens)
        post_feedback(model["gitlab_token"], feedback)


if __name__ == "__main__":
    main()
